package view;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import bll.OrderValidator;
import dao.BillDao;
import dao.CustomerDao;
import dao.ProductDao;
import dao.OrderDao;
import model.Customer;
import model.Order;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Creaza interfata pentru operatiile permise asupra comenzilor.
 */
public class OrderGui {
    private OrderDao daoOrder;
    private Object[][] data;
    private String[] columnNames = new String[Order.class.getDeclaredFields().length];
    private DefaultTableModel model;


    public OrderGui(){
        createWindow();
        daoOrder = new OrderDao();
    }

    private void createWindow(){
        final JFrame frame = new JFrame("OrderGUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setBounds(600, 250, 500, 400);

        //table create
        headerInit();
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(50, 140, 380, 200);
        JTable table = new JTable();
        model = new DefaultTableModel(data, columnNames);
        table.setModel(model);
        scrollPane.setViewportView(table);
        frame.add(scrollPane);

        //----------------------------------create order----------------------------------
        JLabel lInsert = new JLabel("Insert order:");
        lInsert.setBounds(10, 20, 70, 20);
        frame.add(lInsert);
        final JTextField insertIdClient = new JTextField("client id");
        insertIdClient.setBounds(90, 20, 70, 20);
        frame.add(insertIdClient);
        final JTextField insertIdProduct = new JTextField("product id");
        insertIdProduct.setBounds(170, 20, 70, 20);
        frame.add(insertIdProduct);
        final JTextField insertQuantity = new JTextField("quantity");
        insertQuantity.setBounds(250, 20, 70,20);
        frame.add(insertQuantity);
        JButton btnInsert = new JButton("Create order");
        btnInsert.setBounds(350, 20, 110, 20);
        btnInsert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProductDao daoProduct = new ProductDao();
                CustomerDao daoCustomer = new CustomerDao();
                model.setRowCount(0);
                int idOrder = daoOrder.getLastOrderId()+1;
                int idProduct = Integer.parseInt(insertIdProduct.getText());
                int idClient = Integer.parseInt(insertIdClient.getText());
                int quantity = Integer.parseInt(insertQuantity.getText());
                try {
                    Product product = daoProduct.selectById(idProduct);
                    Customer client = daoCustomer.selectById(idClient);
                    Order order = new Order(idOrder, client, product, quantity);
                    OrderValidator ov = new OrderValidator();
                    ov.validate(order);
                    daoOrder.insertNewOrder(order);
                    product.setQuantity(product.getCantitate()-quantity);
                    daoProduct.updateProduct(product);
                    refreshTable();
                } catch (SQLException e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage());
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage());
                }
            }
        });
        frame.add(btnInsert);
        //------------------------------------------generatePDF bill ------------------------------------
        JLabel lGenerate = new JLabel("Generate bill for orderId:");
        lGenerate.setBounds(10,70,150, 20);
        frame.add(lGenerate);
        final JTextField tfGenerateId = new JTextField("1");
        tfGenerateId.setBounds(170, 70, 50, 20);
        frame.add(tfGenerateId);
        JButton btnGenerate = new JButton("Generate PDF");
        btnGenerate.setBounds(250, 70,150,20);
        btnGenerate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int orderId = Integer.parseInt(tfGenerateId.getText());
                generateBill(orderId);
                BillDao bd = new BillDao();
                try {
                    Order order = daoOrder.selectById(orderId);
                    String adress = order.getClient().getAdress();
                    bd.insert(orderId, adress);
                    generateBill(orderId);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
        });
        frame.add(btnGenerate);
        //------------------------------------------refreshTable-----------------------------------------
        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setBounds(220,340,80,20);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                refreshTable();
            }
        });
        frame.add(btnRefresh);
        frame.setVisible(true);
    }
    /**
     * Initializeaza head-ul tabelului cu campurile Clasei Order.
     * Foloseste tehnica reflexiei.
     */
    private void headerInit() {
        Order order = new Order();
        int i = 0;
        for (Field field : order.getClass().getDeclaredFields()) {
            columnNames[i++] = field.getName();
        }
    }

    /**
     * Actualizeaza tabelul cu datele din DB.
     */
    private void refreshTable() {
        ArrayList<Order> orders = daoOrder.selectAll();
        Object[] ordersDetails = new Object[Order.class.getDeclaredFields().length];
        for (Order currOrder : orders) {
            ordersDetails[0] = Integer.toString(currOrder.getId());
            ordersDetails[1] = Integer.toString(currOrder.getClient().getId());
            ordersDetails[2] = Integer.toString(currOrder.getProducts().getId());
            ordersDetails[3] = Integer.toString(currOrder.getQuantityOrdered());
            model.addRow(ordersDetails);
        }
    }

    /**
     * Genereaza o factura sub format PDF.
     * @param orderId id-ul pentru care se genereaza factura.
     */
    private void generateBill(int orderId){
        PdfDocument pdfDocument = null;
        try {
            pdfDocument = new PdfDocument(new PdfWriter("Bill_"+orderId+".pdf"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            int no = 1;
            Order order = daoOrder.selectById(orderId);
            Document layoutDocument = new Document(pdfDocument);
            layoutDocument.add(new Paragraph("INVOICE").setBold().setUnderline().setTextAlignment(TextAlignment.CENTER));

            layoutDocument.add(new Paragraph(order.getClient().getName()).setTextAlignment(TextAlignment.RIGHT).setMultipliedLeading(0.2f));
            layoutDocument.add(new Paragraph(order.getClient().getAdress()).setTextAlignment(TextAlignment.RIGHT).setMultipliedLeading(.2f));
            layoutDocument.add(new Paragraph(order.getClient().getEmail()).setTextAlignment(TextAlignment.RIGHT).setMultipliedLeading(.2f));
            layoutDocument.add(new Paragraph("\n\n\n"));
            Table table = new Table(UnitValue.createPointArray(new float[]{60f, 180f, 50f, 80f}));
            table.addCell(new Paragraph("NO").setBold());
            table.addCell(new Paragraph("Product Name").setBold());
            table.addCell(new Paragraph("Quantity").setBold());
            table.addCell(new Paragraph("Price").setBold());

            table.addCell(new Paragraph(no++ +""));
            table.addCell(new Paragraph(order.getProducts().getName()));
            table.addCell(new Paragraph(Integer.toString(order.getQuantityOrdered())));
            table.addCell(new Paragraph(Double.toString(order.getProducts().getPrice())));

            table.addCell(new Paragraph(""));
            table.addCell(new Paragraph(""));
            table.addCell(new Paragraph("Total:").setBold());
            table.addCell(new Paragraph(Double.toString(order.getQuantityOrdered()*order.getProducts().getPrice())));

            layoutDocument.add(table);

            layoutDocument.add(new Paragraph("\n\n\n\n\n\n"));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            layoutDocument.add(new Paragraph(dtf.format(now)).setTextAlignment((TextAlignment.RIGHT)));
            layoutDocument.close();

        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
}
