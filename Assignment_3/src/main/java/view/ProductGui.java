package view;

import dao.ProductDao;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * Creaza frame-ul aplicatiei. Datele din DB vor fi afisate prin intermediul unui JTable.
 *
 */
public class ProductGui {
    public static Connection connection = null;
    private Object[][] data;
    private String[] columnNames = new String[Product.class.getDeclaredFields().length];
    private JTable table;
    private DefaultTableModel model;
    private ProductDao dao;
    private JFrame frame;

    public ProductGui() {
        createWindow();
        dao = new ProductDao();
    }

    private void createWindow() {
        frame = new JFrame("ProductGUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setBounds(100, 200, 500, 500);

        //table create
        headerInit();
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(50, 220, 380, 200);
        table = new JTable();
        model = new DefaultTableModel(data, columnNames);
        table.setModel(model);
        scrollPane.setViewportView(table);
        frame.add(scrollPane);

        //----------------------------------insert product----------------------------------
        JLabel lInsert = new JLabel("Insert product:");
        lInsert.setBounds(10, 20, 70, 20);
        frame.add(lInsert);
        final JTextField tfIdInsert = new JTextField();
        tfIdInsert.setText("ID");
        tfIdInsert.setBounds(90, 20, 40, 20);
        frame.add(tfIdInsert);
        final JTextField tfNameInsert = new JTextField("product name");
        tfNameInsert.setBounds(140, 20, 100, 20);
        frame.add(tfNameInsert);
        final JTextField tfQuantityInsert = new JTextField("quantity");
        tfQuantityInsert.setBounds(250, 20, 120, 20);
        frame.add(tfQuantityInsert);
        final JTextField tfPriceInsert = new JTextField("price/unit");
        tfPriceInsert.setBounds(380, 20, 100,20);
        frame.add(tfPriceInsert);
        JButton btnInsert = new JButton("Insert");
        btnInsert.setBounds(40, 140, 90, 20);
        btnInsert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdInsert.getText());
                String name = tfNameInsert.getText();
                int quantity = Integer.parseInt(tfQuantityInsert.getText());
                double price = Double.parseDouble(tfPriceInsert.getText());
                Product newProduct = new Product(id, name, quantity, price);
                if (quantity < 0)
                    JOptionPane.showMessageDialog(frame, "Quantity should be greater than 0:" + id);
                else {
                    try {
                        dao.insertNewProduct(newProduct);
                        //refresh Table
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(frame, e1.getMessage());
                    }
                    refreshTable();
                }
            }
        });
        frame.add(btnInsert);
        //-------------------------------update product-----------------------------
        JLabel lUpdate = new JLabel("Update product:");
        lUpdate.setBounds(10, 60, 80, 20);
        frame.add(lUpdate);
        final JTextField tfIdUpdate = new JTextField("ID");
        tfIdUpdate.setBounds(90, 60, 40, 20);
        frame.add(tfIdUpdate);
        final JTextField tfNameUpdate = new JTextField("product name");
        tfNameUpdate.setBounds(140, 60, 100, 20);
        frame.add(tfNameUpdate);
        final JTextField tfQuantityUpdate = new JTextField("quantity");
        tfQuantityUpdate.setBounds(250, 60, 120, 20);
        frame.add(tfQuantityUpdate);
        final JTextField tfPriceUpdate = new JTextField("price/unit");
        tfPriceUpdate.setBounds(380, 60, 100,20);
        frame.add(tfPriceUpdate);
        JButton btnUpdate = new JButton("Update");
        btnUpdate.setBounds(195, 140, 90, 20);
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdUpdate.getText());
                try {
                    Product updateProduct = dao.selectById(id);
                    if (!tfNameUpdate.getText().equals("product name"))
                        updateProduct.setName(tfNameUpdate.getText());
                    if (!tfQuantityUpdate.getText().equals("quantity"))
                        updateProduct.setQuantity(Integer.parseInt(tfQuantityUpdate.getText()));
                    if (!tfPriceUpdate.getText().equals("price/unit"))
                        updateProduct.setPrice(Double.parseDouble(tfPriceUpdate.getText()));
                    dao.updateProduct(updateProduct);
                    //refresh Table
                } catch (SQLException e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage());
                }
                refreshTable();
            }

        });
        frame.add(btnUpdate);
        //-------------------------delete product----------------------------------
        JLabel lDelete = new JLabel("Delete product:");
        lDelete.setBounds(10, 100, 80, 20);
        frame.add(lDelete);
        final JTextField tfIdDelete = new JTextField("ID");
        tfIdDelete.setBounds(90, 100, 80, 20);
        frame.add(tfIdDelete);
        JButton btnDelete = new JButton("Delete");
        btnDelete.setBounds(350, 140, 90, 20);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdDelete.getText());
                dao.deleteProduct(id);
                refreshTable();
            }
        });
        frame.add(btnDelete);

        //------------------------------refreshTable--------------------------------
        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setBounds(220,440,80,20);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                refreshTable();
            }
        });
        frame.add(btnRefresh);

        frame.setVisible(true);

    }

    /**
     * Initializeaza head-ul tabelului cu campurile Clasei Product.
     * Foloseste tehnica reflexiei.
     */
    private void headerInit() {
        Product product = new Product();
        int i = 0;
        for (Field field : product.getClass().getDeclaredFields()) {
            columnNames[i++] = field.getName();
        }

    }

    /**
     * Actualizeaza tabelul cu datele din DB.
     */
    private void refreshTable() {
        ArrayList<Product> products = dao.selectAll();
        Object[] productsDetails = new Object[Product.class.getDeclaredFields().length];
        for (Product currProduct : products) {
            int i = 0;
            for (Field field : currProduct.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    productsDetails[i++] = field.get(currProduct);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            model.addRow(productsDetails);
        }
    }
}
