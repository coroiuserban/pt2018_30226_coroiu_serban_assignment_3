package view;

import bll.CustomerValidator;
import dao.CustomerDao;
import model.Customer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Creaza interfata pentru operatiile permise asupra clientilor.
 */
public class CustomerGUI {
    private Object[][] data;
    private String[] columnNames = new String[4];
    private DefaultTableModel model;
    private CustomerDao dao;
    private JFrame frame;

    public CustomerGUI() {
        createWindow();
        dao = new CustomerDao();
    }

    /**
     * Creaza frame-ul aplicatiei. Datele din DB vor fi afisate prin intermediul unui JTable.
     *
     */
    private void createWindow() {
        frame = new JFrame("CustomerGUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setBounds(1100, 200, 500, 500);

        //table create
        headerInit();
        JTable table;
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(50, 220, 380, 200);
        table = new JTable();
        model = new DefaultTableModel(data, columnNames);
        table.setModel(model);
        scrollPane.setViewportView(table);
        frame.add(scrollPane);

        //-------------------------------------------insert----------------------------------
        JLabel lInsert = new JLabel("Insert client:");
        lInsert.setBounds(10, 20, 70, 20);
        frame.add(lInsert);
        final JTextField tfIdInsert = new JTextField();
        tfIdInsert.setText("ID");
        tfIdInsert.setBounds(90, 20, 40, 20);
        frame.add(tfIdInsert);
        final JTextField tfNameInsert = new JTextField("name");
        tfNameInsert.setBounds(140, 20, 100, 20);
        frame.add(tfNameInsert);
        final JTextField tfAdressInsert = new JTextField("adress");
        tfAdressInsert.setBounds(250, 20, 120, 20);
        frame.add(tfAdressInsert);
        final JTextField tfEmailInsert = new JTextField("email");
        tfEmailInsert.setBounds(380, 20, 110, 20);
        frame.add(tfEmailInsert);
        JButton btnInsert = new JButton("Insert");
        btnInsert.setBounds(40, 140, 90, 20);
        btnInsert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdInsert.getText());
                String name = tfNameInsert.getText();
                String adress = tfAdressInsert.getText();
                String email = tfEmailInsert.getText();
                Customer insertClient = new Customer(id, name, adress, email);
                CustomerValidator customerValidator = new CustomerValidator();
                try {
                    customerValidator.validate(insertClient);
                    dao.insertNewClient(insertClient);
                    refreshTable();
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage());
                }
            }

        });
        frame.add(btnInsert);
        //-------------------------------update client-----------------------------
        JLabel lUpdate = new JLabel("Update client:");
        lUpdate.setBounds(10, 60, 80, 20);
        frame.add(lUpdate);
        final JTextField tfIdUpdate = new JTextField("ID");
        tfIdUpdate.setBounds(90, 60, 40, 20);
        frame.add(tfIdUpdate);
        final JTextField tfNameUpdate = new JTextField("name");
        tfNameUpdate.setBounds(140, 60, 100, 20);
        frame.add(tfNameUpdate);
        final JTextField tfAdressUpdate = new JTextField("adress");
        tfAdressUpdate.setBounds(250, 60, 120, 20);
        frame.add(tfAdressUpdate);
        final JTextField tfEmailUpdate = new JTextField("email");
        tfEmailUpdate.setBounds(380, 60, 110, 20);
        frame.add(tfEmailUpdate);
        JButton btnUpdate = new JButton("Update");
        btnUpdate.setBounds(195, 140, 90, 20);
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdUpdate.getText());
                String name = tfNameUpdate.getText();
                String adress = tfAdressUpdate.getText();
                String email = tfEmailUpdate.getText();
                CustomerValidator customerValidator = new CustomerValidator();
                try {
                    Customer updateClient = dao.selectById(id);
                    if(!name.equals("name"))
                        updateClient.setName(name);
                    if(!adress.equals("adress"))
                        updateClient.setAdress(adress);
                    if(!email.equals("email"))
                        updateClient.setEmail(email);
                    System.out.println(updateClient.getId()+" "+updateClient.getAdress()+" "+updateClient.getEmail()+" "+updateClient.getName());
                    customerValidator.validate(updateClient);
                    dao.updateClient(updateClient);
                    refreshTable();
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage());
                }
            }
        });
        frame.add(btnUpdate);
        //-------------------------delete client----------------------------------
        JLabel lDelete = new JLabel("Delete client:");
        lDelete.setBounds(10, 100, 80, 20);
        frame.add(lDelete);
        final JTextField tfIdDelete = new JTextField("ID");
        tfIdDelete.setBounds(90, 100, 80, 20);
        frame.add(tfIdDelete);
        JButton btnDelete = new JButton("Delete");
        btnDelete.setBounds(350, 140, 90, 20);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int id = Integer.parseInt(tfIdDelete.getText());
                dao.deleteClient(id);
                refreshTable();
            }
        });
        //------------------------------refreshTable--------------------------------
        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setBounds(220, 440, 80, 20);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                refreshTable();
            }
        });
        frame.add(btnRefresh);
        frame.add(btnDelete);
        frame.setVisible(true);

    }

    /**
     * Initializeaza head-ul tabelului cu campurile Clasei Customer.
     * Foloseste tehnica reflexiei.
     */
    private void headerInit() {
        Customer client = new Customer();
        int i = 0;
        for (Field field : client.getClass().getDeclaredFields()) {
            columnNames[i++] = field.getName();
        }

    }

    /**
     * Actualizeaza tabelul cu datele din DB.
     */
    private void refreshTable() {
        ArrayList<Customer> clients = dao.selectAll();
        Object[] clientDetails = new Object[Customer.class.getDeclaredFields().length];
        for (Customer currClient : clients) {
            int i = 0;
            for (Field field : currClient.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    clientDetails[i++] = field.get(currClient);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            model.addRow(clientDetails);
        }
    }

}
