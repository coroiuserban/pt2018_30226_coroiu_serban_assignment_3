package bll;

import model.Customer;
import org.apache.commons.validator.routines.EmailValidator;

/**
 *  Aceasta clasa valideaza inserarea unui client in baza de date. Acesta trebuie sa aiba o
 *  adresa valida, iar email-ul acestuia sa respecte formatul unui email: sa contina @domeniu.tara
 *  Daca clientul nu este validat, metoda va arunca o exceptie.
 */
public class CustomerValidator implements Validator<Customer>{
    private boolean ok;
    public CustomerValidator(){
        ok = true;
    }
    @Override
    /**
     * Metoda validate are:
     * @param obj Acesta este obiectul de tip Client ce urmeaza a fi supus validarii
     * @return boolean Returneaza true daca validarea s-a facut cu succes.
     */
    public boolean validate(Customer obj) throws Exception {
        //id, name, adress, email
        if(obj.getAdress().equals("")) {
            ok = false;
            throw new Exception("Adress could not be empty");
        }
        if(!EmailValidator.getInstance().isValid(obj.getEmail())) {
            ok = false;
            throw new Exception("Incorrect email adress");
        }
        return ok;
    }
}
