package bll;

/**
 * Interfata obliga clasele ce o implementeaza sa suprascrie metoda validate
 * @param <T> Obiectul pe care se va face validarea
 */
public interface Validator<T> {
    public boolean validate(T obj) throws Exception;
}
