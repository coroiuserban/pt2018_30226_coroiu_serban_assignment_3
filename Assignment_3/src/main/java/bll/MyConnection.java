package bll;

import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;

import java.sql.*;

/**
 * Realizeaza conexiunea cu baza de date.
 */
public class MyConnection {
    private static Connection connection;
    public MyConnection(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys", "root", "123456");
        }catch (Exception e){
            e.printStackTrace();
        }
        CustomerDao.setConnection(connection);
        ProductDao.setConnection(connection);
        OrderDao.setConnection(connection);
    }

    /**
     *
     * @return conexiunea la baza de date.
     */
    public static Connection getConnection(){
        return connection;
    }

}
