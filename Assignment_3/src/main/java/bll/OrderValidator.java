package bll;

import model.Order;
import model.Product;

/**
 * Aceasta clasa valideaza inserarea unei comenzi in baza de date. O comanda este valida daca in depozit avem disponibila o cantitate mai mare decat cea ceruta de client.
 * Daca comanda nu este validata, metoda va arunca o exceptie.
 */
public class OrderValidator implements Validator<Order>{
    private boolean ok;
    public OrderValidator(){
        ok = true;
    }

    /**
     *
     * @param obj Primeste un obiect de tipul Order.
     * @return boolean  true daca validarea a avut succes.
     * @throws Exception in caz de eroare se genereaza un mesaj ce se transmite prin intermediul unei exceptii.
     */
    @Override
    public boolean validate(Order obj) throws Exception {
        Product product = obj.getProducts();
        if(obj.getQuantityOrdered() > product.getCantitate()) {
            ok = false;
            throw new Exception("Understock. We have only: " + product.getCantitate() + " pieces");
        }
        return ok;
    }
}
