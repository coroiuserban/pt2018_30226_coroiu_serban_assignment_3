package dao;

import bll.MyConnection;
import model.Product;

import java.sql.*;
import java.util.ArrayList;

/**
 * Realizeaza interogari asupra tabelei product din DB.
 * Conexiunea este primita de la clasa MyConnection.
 * Operatiile posibile sunt:
 * -introducerea unui nou produs
 * -stergerea unui produs existent
 * -actualizarea unui produs existent
 * -gasirea unui produs cu un anumit id
 */
public class ProductDao {
    public static Connection connection;

    public ProductDao(){
        connection = MyConnection.getConnection();
    }

    /**
     * Efectueaza o interogare asupra DB pentru a selecta toate produsele existente.
     * @return ArrayList produsele din DB.
     */
    public ArrayList<Product> selectAll() {
        try {
            ArrayList<Product>  products = new ArrayList<Product>();
            Statement statement = connection.createStatement();
            String querry = "select * from product";
            ResultSet rs = statement.executeQuery(querry);
            while(rs.next())
                products.add(new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)));
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gaseste produsul cu id-ul dat.
     * @param id id-ul comenzii de gasit.
     * @return produsul cu id-ul dat.
     * @throws SQLException daca gasirea esueaza
     */
    public Product selectById(int id) throws SQLException {
        String querry = "SELECT * FROM product WHERE id= " +id;
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        ResultSet rs = statement.executeQuery();
        rs.beforeFirst();
        rs.next();
        return new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4));
    }

    /**
     * Insereaza un nou produs in DB.
     * @param product detaliile produsului ce urmeaza a fi inserat.
     * @throws SQLException daca inserearea esueaza
     */
    public void insertNewProduct(Product product) throws SQLException {
        String querry = "INSERT INTO product (id, name, cantitate, price) VALUES(?,?,?,?)";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, product.getId());
        statement.setString(2, product.getName());
        statement.setInt(3, product.getCantitate());
        statement.setDouble(4, product.getPrice());
        statement.executeUpdate();
    }

    /**
     * Actualizeaza produsul din DB ce are id-ul primit ca si parametru.
     * @param product produsul ce urmeaza sa fie actualizat.
     * @throws SQLException daca actualizarea esueaza
     */
    public void updateProduct(Product product) throws SQLException {
        String querry = "UPDATE product SET name=?, cantitate=?, price=? WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setString(1, product.getName());
        statement.setInt(2, product.getCantitate());
        statement.setDouble(3, product.getPrice());
        statement.setInt(4, product.getId());
        statement.executeUpdate();

    }

    /**
     * Sterge produsul cu id-ul primit din DB.
     * @param id produsul ce urmeaza sa fie sters din DB.
     */
    public void deleteProduct(int id){
        try{
            String querry = "DELETE FROM product WHERE id =?";
            PreparedStatement statement = null;
            statement = connection.prepareStatement(querry);
            statement.setInt(1, id);
            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    /**
     * Seteaza conexiune spre DB.
     * @param connection conexiunea spre DB
     */
    public static void setConnection(Connection connection) {
        CustomerDao.connection = connection;
    }
}
