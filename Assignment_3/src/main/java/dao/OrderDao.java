package dao;

import model.Customer;
import model.Order;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Realizeaza interogari asupra tabelei Order din DB.
 * Conexiune este primita de la clasa MyConnection.
 * Operatiile posibile sunt:
 * -gasirea tuturor comenzilor
 * -crearea unei noi comenzi
 * -gasirea unei anumite comenzi
 * -gasirea id-ului ultimei comenzi care a fost generata.
 */
public class OrderDao {
    private static Connection connection;
    private ProductDao productDao;
    private CustomerDao customerDao;

    /**
     * Realizeaza conexiuni spre cele 2 clase ce se ocupa de inserarea in tabelele customer si product.
     */
    public OrderDao(){
        productDao = new ProductDao();
        customerDao = new CustomerDao();
    }

    /**
     * Efectueaza o interogare asupra DB pentru a selecta toate comenzile generate.
     * @return ArrayList cu toate comezile din DB
     */
    public ArrayList<Order> selectAll() {
        try {
            ArrayList<Order>  orders = new ArrayList<Order>();
            Statement statement = connection.createStatement();
            String querry = "SELECT * FROM orders";
            ResultSet rs = statement.executeQuery(querry);
            int idOrder, idProduct, idClient;
            while(rs.next()){
                idOrder = rs.getInt(1);
                idClient = rs.getInt(2);
                Customer client = customerDao.selectById(idClient);
                idProduct = rs.getInt(3);
                Product product = productDao.selectById(idProduct);
                //id client product
                orders.add(new Order(idOrder, client, product, rs.getInt(4)));
            }
            return orders;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Selecteaza id-ul ultimei comenzi ce a fost generata.
     * @return id-ul ultimei comenzi
     */
    public int getLastOrderId(){
        ArrayList<Order> orders = selectAll();
        Collections.reverse(orders);
        return orders.get(0).getId();

    }

    /**
     * Creaza o noua comanda in DB.
     * @param order detaliile comenzii ce a fost generata
     * @throws SQLException daca inserarea esueaza
     */
    public void insertNewOrder(Order order) throws SQLException {
        String querry = "INSERT INTO orders (idOrder, idClient, idProduct, quantity) VALUES(?,?,?,?)";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, order.getId());
        statement.setInt(2, order.getClient().getId());
        statement.setInt(3, order.getProducts().getId());
        statement.setInt(4, order.getQuantityOrdered());
        statement.executeUpdate();
    }

    /**
     * Gaseste comanda cu id-ul dat.
     * @param id id-ul comenzii de gasit.
     * @return comanda cu id-ul dat
     * @throws SQLException daca gasirea esueaza
     */
    public Order selectById(int id) throws SQLException {
        String querry = "SELECT * FROM orders WHERE idOrder = " +id;
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        ResultSet rs = statement.executeQuery();
        rs.beforeFirst();
        rs.next();
        Order order = new Order();
        order.setId(rs.getInt(1));
        Customer client = customerDao.selectById(rs.getInt(2));
        Product product = productDao.selectById(rs.getInt(3));
        int quantity = rs.getInt(4);
        order.setClient(client);
        order.setProduct(product);
        order.setQuantityOrdered(quantity);
        return order;
    }

    /**
     *
     * @param connection conexiunea spre DB
     */
    public static void setConnection(Connection connection){ OrderDao.connection = connection; }
}
