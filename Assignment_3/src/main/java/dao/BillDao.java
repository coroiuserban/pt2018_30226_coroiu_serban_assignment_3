package dao;

import bll.MyConnection;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *  Realizeaza interogari asupra bazei de date.
 *  Conexiune cu baza de date este primita din clasa MyConnection
 */
public class BillDao {
    public static Connection connection;

    public BillDao(){
        connection = MyConnection.getConnection();
    }

    /**
     * Insereaza in tabelul invoice date despre comanda pentru care a fost generata factura.
     * @param orderId id-ul pentru care se genereaza factura
     * @param adress adresa unde va fi livrat produsului
     * @throws SQLException in cazul in care inserarea esueaza
     */
    public void insert(int orderId, String adress) throws SQLException {

        String querry = "INSERT INTO invoice (idOrder, dateGenerated, shippingAdress, estimatedDelivery) VALUES(?,?,?,?)";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, orderId);
        Date currDate = new Date();
        statement.setString(2, currDate.toString());
        statement.setString(3,adress);
        Calendar c = Calendar.getInstance();
        c.setTime(currDate);
        c.add(Calendar.DATE, 14);
        currDate = c.getTime();
        statement.setString(4, currDate.toString());
        statement.executeUpdate();
    }
}
