package dao;

import bll.MyConnection;
import model.Customer;
import java.sql.*;
import java.util.ArrayList;

/**
 * Realizeaza interogari asupra tabelei Customer,
 * din baza de date a carei conexiune este primita din clasa MyConnection.
 * Operatiile posibile sunt:
 * -gasirea tuturor clientilor
 * -inserarea unui client noi
 * -stergerea unui client existent
 * -actualizarea unui client existent
 * -gasirea unui anumit client cu un id dat
 */
public class CustomerDao {
    public static Connection connection;

    public CustomerDao(){
        connection = MyConnection.getConnection();
    }

    /**
     * Efectueaza o interogare asupra DB pentru a selecta toti clientii.
     * @return ArrayList cu toti clientii din baza de date.
     */
    public ArrayList<Customer> selectAll() {
        try {
            ArrayList<Customer>  clients = new ArrayList<Customer>();
            Statement statement = connection.createStatement();
            String querry = "select * from customer";
            ResultSet rs = statement.executeQuery(querry);
            while(rs.next())
                clients.add(new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            return clients;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Insereaza clientul primit ca si parametru in DB
     * @param client clientul ce urmeaza a fi inserat in DB.
     * @throws SQLException daca inserarea esueaza
     */
    public void insertNewClient(Customer client) throws SQLException {

        String querry = "INSERT INTO customer (id, name, adress, email) VALUES(?,?,?,?)";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, client.getId());
        statement.setString(2, client.getName());
        statement.setString(3, client.getAdress());
        statement.setString(4, client.getEmail());
        statement.executeUpdate();


    }

    /**
     * Actualizeaza un client din DB ce are id-ul primit ca si parametru.
     * @param client clientul ce urmeaza sa fie actualizat.
     * @throws SQLException daca actualizarea esueaza
     */
    public void updateClient(Customer client) throws SQLException {
        String querry = "UPDATE customer SET name=?, adress=?, email=? WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setString(1, client.getName());
        statement.setString(2, client.getAdress());
        statement.setString(3, client.getEmail());
        statement.setInt(4, client.getId());
        statement.executeUpdate();

    }

    /**
     * Sterge clientul cu id-ul primit din DB.
     * @param id clientul ce urmeaza sa fie sters din DB.
     */
    public void deleteClient(int id){
        try{
            String querry = "DELETE FROM customer WHERE id =?";
            PreparedStatement statement = null;
            statement = connection.prepareStatement(querry);
            statement.setInt(1, id);
            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Gaseste detaliile clientului cu id-ul dat ca si parametru din DB.
     * @param id clientul care trebuie cautat.
     * @return client-ul cu id dat.
     * @throws SQLException daca selectarea esueaza
     */
    public Customer selectById(int id) throws SQLException {
        String querry = "SELECT * FROM customer WHERE id= " +id;
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        ResultSet rs = statement.executeQuery();
        rs.beforeFirst();
        rs.next();
        return new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
    }

    /**
     * Seteaza conexiune spre DB.
     * @param connection conexiunea spre DB
     */
    public static void setConnection(Connection connection) {
        CustomerDao.connection = connection;
    }
}
