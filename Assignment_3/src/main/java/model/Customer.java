package model;

/**
 * Modeleaza un client.
 * Un client este format din:
 * -id
 * -nume
 * -adresa
 * -email
 */
public class Customer {
    private int id;
    private String name;
    private String adress;
    private String email;

    public Customer(){
        id =0;
        name = "";
        adress = "";
    }
    public Customer(int id, String name, String adress){
        this.name = name;
        this.adress = adress;
        this.id = id;
    }
    public Customer(int id, String name, String adress, String email){
        this.name = name;
        this.adress = adress;
        this.id = id;
        this.email = email;
    }

    /**
     * Returneaza numele clientului.
     * @return client name.
     */
    public String getName(){
        return name;
    }

    /**
     * Returneaza adresa clientului.
     * @return client adress.
     */
    public String getAdress(){
        return adress;
    }

    /**
     * Returneaza id-ul clientului.
     * @return client id.
     */
    public int getId(){
        return id;
    }

    /**
     * Returneaza email-ul clientului.
     * @return client email.
     */
    public String getEmail(){ return email;}

    /**
     * Seteaza numele primit ca si parametru clientului.
     * @param name client name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Seteaza adresa primita ca si parametru clientului.
     * @param adress client adress.
     */
    public void setAdress(String adress){
        this.adress = adress;
    }

    /**
     * Seteaza email-ul primit ca si parametru clientului.
     * @param email client email.
     */
    public void setEmail(String email){
        this.email = email;
    }
}
