package model;

/**
 * Modeleaza un produs.
 * Un produs este format din:
 * -id
 * -nume
 * -cantitate disponibila
 * -pret per bucata
 */
public class Product {
    private int id;
    private String name;
    private int cantitate;
    private double price;
    public Product(int id, String name, int cantitate){
        this.id = id;
        this.cantitate = cantitate;
        this.name = name;
    }

    public Product(int id, String name, int cantitate, double price){
        this.id = id;
        this.cantitate = cantitate;
        this.name = name;
        this.price = price;
    }

    public Product(){
        id=0;
        name="";
        cantitate=-1;
    }

    /**
     * Returneaza numele produsului
     * @return product name
     */
    public String getName(){
        return name;
    }

    /**
     * Returneaza id-ul produsului.
     * @return product id
     */
    public int getId(){ return id;  }

    /**
     * Returneaza cantitatea disponibila a produsului.
     * @return product available quantity
     */
    public int getCantitate(){
        return cantitate;
    }

    /**
     * Returneaza pretul produsului.
     * @return product price.
     */
    public double getPrice(){ return price; }

    /**
     * Seteaza numele primit ca si parametru produsului.
     * @param name client name.
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Seteaza cantitatea primita ca si parametru produsului.
     * @param quantity available product quantity
     */
    public void setQuantity(int quantity){
        this.cantitate = quantity;
    }

    /**
     * Seteaza pretul primit ca si parametru produsului.
     * @param price product price.
     */
    public void setPrice(double price){
        this.price = price;
    }
}
