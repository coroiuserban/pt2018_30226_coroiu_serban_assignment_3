package model;

/**
 * Modeleaza o comanda.
 * O comanda are:
 * -id
 * -un client care a facut comanda respectiva
 * -produsul dorit de client
 * -cantitatea dorita de client
 */
public class Order {
    private int id;
    private Customer client;
    private Product product;
    private int quantityOrdered;

    public Order(){
        product = new Product();
        id = 0;
        client = new Customer();
    }

    public Order(int id, Customer client, Product product, int quantityOrdered){
        this.product = product;
        this.client = client;
        this.id = id;
        this.quantityOrdered = quantityOrdered;
    }

    /**
     * Returneaza produsului din comanda.
     * @return produsul din comanda.
     */
    public Product getProducts(){
        return product;
    }

    /**
     * Returneaza clientul din comanda.
     * @return clientul din comanda.
     */
    public Customer getClient(){
        return client;
    }

    /**
     * Returneaza id-ul comenzii.
     * @return order id;
     */
    public int getId(){
        return id;
    }

    /**
     * Returneaza cantitatea din comanda.
     * @return cantitatea din comanda.
     */
    public int getQuantityOrdered(){    return quantityOrdered; }

    /**
     * Seteaza comenzii id-ul primit ca si parametru.
     * @param id noul id.
     */
    public void setId(int id){
        this.id = id;
    }

    /**
     * Seteaza comenzii clientul primit ca si parametru.
     * @param client noul client.
     */
    public void setClient(Customer client){
        this.client = client;
    }
    /**
     * Seteaza comenzii produsul primit ca si parametru.
     * @param product noul produs.
     */
    public void setProduct(Product product){
        this.product = product;
    }

    /**
     * Seteaza comenzii cantitatea primita ca si parametru.
     * @param quantityOrdered noua cantitate dorita.
     */
    public void setQuantityOrdered(int quantityOrdered){
        this.quantityOrdered =quantityOrdered;
    }
}
