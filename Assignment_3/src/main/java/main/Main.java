package main;


import bll.MyConnection;
import view.CustomerGUI;
import view.OrderGui;
import view.ProductGui;
import java.sql.SQLException;

/**
 * Created by Serban Coroiu
 * Clasa main are rolul de a porni aplicatia, fiind clasa ce este rulata implicit de catre compilator.
 * In aceasta clasa avem 4 obiecte instantiate, adica cele 3 interfete grafice si conexiunea cu baza de date.
 */

public class Main {
    public static void main(String[] args) throws SQLException {

        MyConnection myConnection = new MyConnection();
        CustomerGUI customerGUI = new CustomerGUI();
        ProductGui productGui = new ProductGui();
        OrderGui orderGui = new OrderGui();

    }
}
